var fs  = require('fs-extra');

angular.module('main').service('creator', function(){
    'use strict';

    return {
        CreateDir: function(path, overwrite, sync)
        {
            try
            {
                fs.mkdirSync(path);
                return [true];
            }
            catch(err)
            {
                if(err.code === "EEXIST")
                {
                    if(overwrite && overwrite === true)
                    {
                        fs.rmdirSync(path);
                        this.CreateDir(path, false);
                    }
                    else
                    {
                        return [true];
                    }
                }
                else
                {
                    return [false, err];
                }
            }
        },
        CreateFile: function(path, content)
        {
            try
            {
                var file = fs.openSync(path,"w");
                if(content)
                {
                    if(typeof content !== "string")
                    {
                        content = JSON.stringify(content, null ,4);
                    }
                    var buffer = new Buffer(content);
                    fs.writeSync(file, buffer, 0,buffer.length);
                }
            }
            catch(err)
            {
                return [false, err];
            }
        },
        CopyFile: function(source, dest)
        {
            try
            {
                fs.copySync(source, dest);
            }
            catch(err)
            {
                console.log(err);
            }
        }
    };
});