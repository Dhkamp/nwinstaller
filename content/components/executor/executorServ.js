var child_process = require('child_process');

angular.module('main').service('executor', function(){
    'use strict';

    return {
        Execute: function(path, hidden)
        {
            try
            {
                var _process = child_process.execFile(path, function(err, stdout, stderr)
                {
                    if(err)
                    {
                        return [false, err];
                    }
                });
                return [true];
            }
            catch(err)
            {
                return [false, err];
            }

        }
    }
});