angular.module('main').controller('installerCtrl', ['$scope', 'creator','executor', function($scope, creator, executor){

	var installSettings = {
        "path": "",
        "dirs": [
            { "path": "convert" },
            { "path": "logs" }
        ],
        "files": [
            {
                "path": "test.json",
                "content":"asdasd"
            },
            {
                "path": "logs\\test2.json",
                "content": "teststring"
            }
        ],
        "executables" : [
            { "exe" : "executables\\switch.exe"},
            { "exe" : "thunderbird.exe" }
        ],
        "filesToCopy": [
            {
                "name": "ffmpegsumo.dll",
                "path": "./content/data/ffmpegsumo.dll"
            },
            {
                "name": "icudt.dll",
                "path": "./content/data/icudt.dll"
            },
            {
                "name": "libEGL.dll",
                "path": "./content/data/libEGL.dll"
            },
            {
                "name": "libGLESv2.dll",
                "path": "./content/data/libGLESv2.dll"
            },
            {
                "name": "nw.pak",
                "path": "./content/data/nw.pak"
            },
            {
                "name": "easyDikt.exe",
                "path": "./content/data/easyDikt.exe"
            }
        ],
        "createShortCut": "true"
    }

    $scope.installPath          = "C:\\easyDikt";
    $scope.message              = "";
    $scope.msgClass             = 'message-regular';
    $scope.progress             = "";
    $scope.progressClass        = "";
    $scope.installInProgress    = "install-false";

    $scope.doInstall = function()
    {
        $scope.installInProgress = "install-true";

        installSettings.path = $scope.installPath;
        creator.CreateDir(installSettings.path, false);

        createFolders();
        createFiles();
        copyFiles();

        window.setTimeout(function(){
            location.href = "#/endInstall";
        }, 2000);
    };

    function createFolders()
    {
        for (var i = 0, j = installSettings.dirs.length; i < j; i++)
        {
            creator.CreateDir(installSettings.path + "\\" + installSettings.dirs[i].path);
        }
    }

    function createFiles()
    {
    	for (var i = 0, j = installSettings.files.length; i < j; i++)
    	{
    		creator.CreateFile(installSettings.path + "//" + installSettings.files[i].path, installSettings.files[i].content);
    	}  	
    }

    function copyFiles()
    {
        for( var i = 0, j = installSettings.filesToCopy.length; i < j; i++)
        {
            var file = installSettings.filesToCopy[i];
            creator.CopyFile(file.path, installSettings.path + "\\" + file.name);
        }
    }

    $scope.closeInstaller =function()
    {

    }

    $scope.installExternals = function(path)
    {
        executor.Execute(path, false);
    }

    $scope.CreateStructure = function()
    {
    	createFolders();
    	createFiles();
    }


}]);