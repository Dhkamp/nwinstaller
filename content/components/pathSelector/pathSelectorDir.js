angular.module('main').directive('pathSelector', function(){

    'use strict';

    return{
        restrict: 'E',
        /*scope:{
            default:'='
        },*/
        template: '<div class="input-group ed-settings-group">'                                 +
            '<input style="display: none" class="ed-dir-filepath-real" type="file">'              +
            '<span class="input-group-btn">'                                            +
            '<button class="btn btn-default" type="button">Durchsuchen</button>'    +
            '</span>'                                                                   +
            '<input type="text" class="form-control" ng-model="installPath">'               +
            '</div>',
        link: function($scope, $element, $attrs)
        {
            var nChildren = $element.children().children();

            var _fileIn     = nChildren[0];
            var _button     = nChildren.children()[0];
            //var _textOut    = nChildren[2];

            function addAttributes()
            {
                if($attrs.isdir && $attrs.isdir === "true")
                {
                    _fileIn.setAttribute('nwdirectory', '')
                }
                if($attrs.dir)
                {
                    _fileIn.setAttribute('nwworkingdir', $attrs.dir);
                }
            }

            function addListeners()
            {
                _button.addEventListener('click', function(e)
                {
                    _fileIn.click();
                });

                _fileIn.addEventListener('change', function(e)
                {
                    $scope.$apply(function()
                    {
                        $scope.$parent[$attrs.default] = _fileIn.value;
                    });
                });
            }

            //Execute this function after Load
            addAttributes();
            addListeners();
        }
    }
});