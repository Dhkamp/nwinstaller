
//var install 	= require("./data/install.json");

var mainModule = angular.module('main', ['ui.router']);

mainModule.config(function ($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise("/welcome");

    $stateProvider
        .state('Welcome', {
            url: "/welcome",
            templateUrl: "routes/welcome.html"
        })
        .state('SetInstallDir', {
            url: "/setInstallDir",
            templateUrl: "routes/setInstallDir.html"
            //controller: 'installerCtrl'
        })
        .state('InstallExternals', {
            url: "/installExternals",
            templateUrl: "routes/installExternals.html"
            //controller: 'installerCtrl'
        })
        .state('EndInstall', {
            url: "/endInstall",
            templateUrl: "routes/endInstall.html"
            //controller: 'installerCtrl'
        })
});